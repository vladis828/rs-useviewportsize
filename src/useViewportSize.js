import { useState, useEffect } from 'react';

export function useViewportSize() {
  const [height, setHeight] = useState(() => window.innerHeight);
  const [width, setWidth] = useState(() => window.innerWidth);

  const heightChangeHandler = (height) => {
    setHeight(height);
  };

  const widthChangeHandler = (width) => {
    setWidth(() => width);
  };

  useEffect(() => {
    window.addEventListener('resize', () => {
      const width = window.innerWidth;
      const height = window.innerHeight;
      widthChangeHandler(width);
      heightChangeHandler(height);
    });

    return () => {
      window.removeEventListener(
        'resize',
        widthChangeHandler,
        heightChangeHandler
      );
    };
  }, []);

  return { height, width };
}
